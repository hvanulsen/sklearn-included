import sklearn
import shap
import boto3
import dill
import joblib
import datetime
from io import BytesIO
import json

# Load data
X,y = shap.datasets.adult()
X_display,y_display = shap.datasets.adult(display=True)
X_train, X_valid, y_train, y_valid = sklearn.model_selection.train_test_split(X, y, test_size=0.2, random_state=7)

# Train knn
knn = sklearn.neighbors.KNeighborsClassifier()
knn.fit(X_train, y_train)

# Fit Shap Kernel Explainer
f = lambda x: knn.predict_proba(x)[:,1]
med = X_train.median().values.reshape((1,X_train.shape[1]))
explainer = shap.KernelExplainer(f, med)

# Export model objects
timestamp_now = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
bucket_model = 'deeploy-examples'
bucket_explainer = 'deeploy-examples'
explainer_object_key = f'sklearn/census/{timestamp_now}/explainer/explainer.dill'
model_object_key = f'sklearn/census/{timestamp_now}/model/model.joblib'


def object_to_s3(bucket, key, object, object_type):
    if object_type == 'explainer':
        print(object)
        with open('explainer.dill', 'wb') as dill_file:
            dill.dump(object, dill_file)
    else:
        joblib.dump(object, 'model.joblib')



object_to_s3(bucket_model, model_object_key, knn, 'model')
object_to_s3(bucket_explainer, explainer_object_key, explainer, 'explainer')

# Add reference to repo
model_reference = {
    'reference': {
        'blob': {
            'url': 's3://' + bucket_explainer + '/sklearn/census/' + timestamp_now + '/model'
            }
        }
    }
explainer_reference = {
    'reference': {
        'blob': {
            'url': 's3://' + bucket_explainer + '/sklearn/census/' + timestamp_now + '/explainer'
            }
        }
    }

# with open('model/reference.json', 'w', encoding='utf-8') as f:
#     json.dump(model_reference, f, ensure_ascii=False, indent=4)
# with open('explainer/reference.json', 'w', encoding='utf-8') as f:
#     json.dump(explainer_reference, f, ensure_ascii=False, indent=4)